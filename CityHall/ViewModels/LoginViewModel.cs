﻿using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.Authentication;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class LoginViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly AuthController authController;

        public string Email { get; set; }
        public string Password { get; set; }

        public LoginViewModel()
        {
            windowManager = new WindowManager();

            IAuthService authService = new AuthService();
            authController = new AuthController(authService);
        }

        public void Login()
        {
            User user = authController.Login(Email, Password);

            if (user == null)
            {
                MessageBox.Show(
                    "No such user could be found!",
                    "Login Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else if (user is Admin)
            {
                windowManager.ShowWindow(new AdminOperationsViewModel());
                TryClose();
            }
            else if (user is RegularUser regularUser)
            {
                windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
                TryClose();
            }
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }

    }
}
