﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using Caliburn.Micro;
using CityHall.BLL.AdminBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class FilterRequestsViewModel : Screen, INotifyPropertyChanged
    {
        private string nameFilter;
        private readonly CollectionViewSource requestCollection;
        public override event PropertyChangedEventHandler PropertyChanged;
        private readonly IWindowManager windowManager;
        private readonly AdminController adminController;

        public string NameFilter
        {
            get => nameFilter;
            set
            {
                nameFilter = value;
                requestCollection.View.Refresh();
                RaisePropertyChanged(nameof(NameFilter));
            }
        }

        public ICollectionView SourceCollection => requestCollection.View;

        public FilterRequestsViewModel()
        {
            windowManager = new WindowManager();

            IAdminService adminService = new AdminService();
            adminController = new AdminController(adminService);

            ObservableCollection<Request> requests = new ObservableCollection<Request>(adminController.GetAllRequests());

            requestCollection = new CollectionViewSource {Source = requests};
            requestCollection.Filter += NameRequestCollectionFilter;
        }

        public void NameRequestCollectionFilter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrEmpty(NameFilter))
            { 
                e.Accepted = true;
                return;
            }
 
            Request request = e.Item as Request;
            e.Accepted = request.User.LastName.ToUpper().Contains(NameFilter.ToUpper());
        }

        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new AdminOperationsViewModel());
            TryClose();
        }
    }
}
