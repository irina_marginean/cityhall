﻿using Caliburn.Micro;
using CityHall.BLL.AdminBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class AllUsersViewModel : Screen
    {
        private readonly IWindowManager windowManager;

        public BindableCollection<User> Users { get; set; }

        public AllUsersViewModel()
        {
            windowManager = new WindowManager();

            IAdminService adminService = new AdminService();
            var adminController = new AdminController(adminService);

            Users = new BindableCollection<User>(adminController.GetAllUsers());
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new AdminOperationsViewModel());
            TryClose();
        }
    }
}
