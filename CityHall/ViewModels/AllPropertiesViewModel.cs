﻿using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.RegularUserBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class AllPropertiesViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly RegularUserController regularUserController;
        private readonly RegularUser regularUser;

        public BindableCollection<Property> Properties { get; set; }

        public Property SelectedProperty { get; set; }

        public AllPropertiesViewModel(RegularUser user)
        {
            regularUser = user;
            windowManager = new WindowManager();

            IRegularUserService regularUserService = new RegularUserService();
            regularUserController = new RegularUserController(regularUserService);

            Properties =
                new BindableCollection<Property>(regularUserController.GetAllProperties(user));
        }

        public void DeleteProperty()
        {
            if (SelectedProperty != null)
            {
                regularUserController.RemoveProperty(regularUser, SelectedProperty);

                MessageBox.Show(
                    "You successfully removed a property!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            else
            {
                MessageBox.Show(
                    "You did not select a property to be removed!",
                    "Error",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
            TryClose();
        }
    }
}
