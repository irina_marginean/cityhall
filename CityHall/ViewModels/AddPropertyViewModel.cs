﻿using System;
using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.RegularUserBusiness;
using CityHall.BLL.Utils;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class AddPropertyViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly RegularUserController regularUserController;
        private readonly RegularUser regularUser;

        public string Street { get; set; }
        public string Number { get; set; }
        public string Apartment { get; set; }

        public AddPropertyViewModel(RegularUser user)
        {
            windowManager =  new WindowManager();

            IRegularUserService regularUserService = new RegularUserService();
            regularUserController = new RegularUserController(regularUserService);

            regularUser = user;
        }

        public void AddProperty()
        {
            try
            {
                Property property = EntityFactory.CreateDefaultProperty(regularUser);
                property.Street = Street;
                property.Number = Number;
                property.Apartment = Apartment;

                regularUserController.AddProperty(property);

                MessageBox.Show(
                    "You successfully added a new property!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "Not enough data!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
            TryClose();
        }
    }
}
