﻿using Caliburn.Micro;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class UserOperationsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly RegularUser regularUser;

        public UserOperationsViewModel(RegularUser user)
        {
            windowManager = new WindowManager();

            regularUser = user;
        }

        public void ViewAllProperties()
        {
            windowManager.ShowWindow(new AllPropertiesViewModel(regularUser));
            TryClose();
        }

        public void AddProperty()
        {
            windowManager.ShowWindow(new AddPropertyViewModel(regularUser));
            TryClose();
        }

        public void ViewAllRequests()
        {
            windowManager.ShowWindow(new AllUserRequestsViewModel(regularUser));
            TryClose();
        }

        public void UpdateRequest()
        {
            windowManager.ShowWindow(new EditUserRequestsViewModel(regularUser));
            TryClose();
        }

        public void Logout()
        {
            windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}
