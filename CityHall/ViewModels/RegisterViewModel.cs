﻿using System;
using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.Authentication;
using CityHall.BLL.Utils;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class RegisterViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private RegularUser regularUser;
        private readonly AuthController authController;

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public RegisterViewModel()
        {
            windowManager = new WindowManager();

            IAuthService authService = new AuthService();
            authController = new AuthController(authService);
        }

        public void Submit()
        {
            try
            {
                regularUser = EntityFactory.CreateDefaultRegularUser();
                regularUser.FirstName = FirstName;
                regularUser.LastName = LastName;
                regularUser.Email = Email;
                regularUser.Password = Password;

                authController.Register(regularUser);

                windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
                TryClose();
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Registration Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        public void Cancel()
        {
            windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}
