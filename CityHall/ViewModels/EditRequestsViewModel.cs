﻿using System;
using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.AdminBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class EditRequestsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly AdminController adminController;

        public BindableCollection<Request> Requests { get; set; }
        public Request SelectedRequest { get; set; }

        public EditRequestsViewModel()
        {
            windowManager = new WindowManager();

            IAdminService adminService = new AdminService();
            adminController = new AdminController(adminService);

            Requests = new BindableCollection<Request>(adminController.GetAllRequests());
        }

        public void ApproveRequest()
        {
            try
            {
                adminController.ApproveRequest(SelectedRequest);

                MessageBox.Show(
                    "You successfully approved the request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "Not request selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void DenyRequest()
        {
            try
            {
                adminController.DenyRequest(SelectedRequest);

                MessageBox.Show(
                    "You successfully denied the request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "No request selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }


        public void DeleteRequest()
        {
            try
            {
                adminController.RemoveRequest(SelectedRequest);

                MessageBox.Show(
                    "You successfully removed the request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "No request selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new AdminOperationsViewModel());
            TryClose();
        }
    }
}
