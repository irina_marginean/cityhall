﻿using Caliburn.Micro;

namespace CityHall.ViewModels
{
    public class AdminOperationsViewModel : Screen
    {
        private readonly IWindowManager windowManager;

        public AdminOperationsViewModel()
        {
            windowManager = new WindowManager();
        }

        public void ViewAllUsers()
        {
            windowManager.ShowWindow(new AllUsersViewModel());
            TryClose();
        }

        public void ViewAllRequests()
        {
            windowManager.ShowWindow(new AllRequestsViewModel());
            TryClose();
        }

        public void EditRequests()
        {
            windowManager.ShowWindow(new EditRequestsViewModel());
            TryClose();
        }

        public void FilterRequests()
        {
            windowManager.ShowWindow(new FilterRequestsViewModel());
            TryClose();
        }

        public void ViewAllDocuments()
        {
            windowManager.ShowWindow(new EditDocumentsViewModel());
            TryClose();
        }

        public void Logout()
        {
            windowManager.ShowWindow(new MainWindowViewModel());
            TryClose();
        }
    }
}
