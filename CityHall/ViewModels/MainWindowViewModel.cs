﻿using Caliburn.Micro;

namespace CityHall.ViewModels
{
    public class MainWindowViewModel : Screen
    {
        private readonly IWindowManager windowManager;

        public MainWindowViewModel()
        {
            windowManager = new WindowManager();
        }

        public void Login()
        {
            windowManager.ShowWindow(new LoginViewModel());
            TryClose();
        }

        public void Register()
        {
            windowManager.ShowWindow(new RegisterViewModel());
            TryClose();
        }
    }
}
