﻿using Caliburn.Micro;
using CityHall.BLL.AdminBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class AllRequestsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly AdminController adminController;

        public BindableCollection<Request> Requests { get; set; }

        public AllRequestsViewModel()
        {
            windowManager = new WindowManager();

            IAdminService adminService = new AdminService();
            adminController = new AdminController(adminService);

            Requests = new BindableCollection<Request>(adminController.GetAllRequests());
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new AdminOperationsViewModel());
            TryClose();
        }
    }
}
