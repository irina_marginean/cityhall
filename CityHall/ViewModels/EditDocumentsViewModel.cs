﻿using System;
using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.AdminBusiness;
using CityHall.BLL.Utils;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class EditDocumentsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly AdminController adminController;

        public string DocumentType { get; set; }
        public Document SelectedDocument { get; set; }
        public BindableCollection<Document> Documents { get; set; }

        public EditDocumentsViewModel()
        {
            windowManager = new WindowManager();

            IAdminService adminService = new AdminService();
            adminController = new AdminController(adminService);

            Documents = new BindableCollection<Document>(adminController.GetAllDocuments());
        }

        public void AddDocument()
        {
            try
            {
                Document document = EntityFactory.CreateDefaultDocument();

                document.Type = DocumentType;

                adminController.AddDocument(document);

                MessageBox.Show(
                    "You successfully added the document!",
                    "Success!",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "Not enough data!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void DeleteDocument()
        {
            try
            {
                adminController.RemoveDocument(SelectedDocument);

                MessageBox.Show(
                    "You successfully deleted the document!",
                    "Success!",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show(
                    "No document was selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }  

        public void GoBack()
        {
            windowManager.ShowWindow(new AdminOperationsViewModel());
            TryClose();
        }
    }
}
