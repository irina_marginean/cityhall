﻿using System;
using System.Windows;
using Caliburn.Micro;
using CityHall.BLL.RegularUserBusiness;
using CityHall.BLL.Utils;
using CityHall.DAL;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class EditUserRequestsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly RegularUser regularUser;
        private readonly RegularUserController regularUserController;

        public BindableCollection<Request> Requests { get; set; }
        public BindableCollection<Document> Documents { get; set; }
        public BindableCollection<Property> Properties { get; set; }
        public Property SelectedProperty { get; set; }
        public Document SelectedDocument { get; set; }
        public Request SelectedRequest { get; set; }

        public EditUserRequestsViewModel(RegularUser regularUser)
        {
            this.regularUser = regularUser;

            windowManager = new WindowManager();

            IRegularUserService regularUserService = new RegularUserService();
            regularUserController = new RegularUserController(regularUserService);

            Requests = new BindableCollection<Request>(regularUserController.GetAllRequests(regularUser));
            Properties = new BindableCollection<Property>(regularUserController.GetAllProperties(regularUser));
            Documents = new BindableCollection<Document>(RepositoryLocator.DocumentRepository.GetAll());
        }

        public void AddRequest()
        {
            try
            {
                Request request = EntityFactory.CreateDefaultRequest(regularUser, SelectedProperty, SelectedDocument);

                regularUserController.AddRequest(request);

                MessageBox.Show(
                    "You successfully added a new request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show(
                    "Not enough data selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Cannot make more requests!",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }

        public void EditRequest()
        {
            try
            {
                SelectedRequest.Property = SelectedProperty;
                SelectedRequest.Document = SelectedDocument;

                regularUserController.UpdateRequest(SelectedRequest);

                MessageBox.Show(
                    "You successfully edited the request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "Not enough data selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(
                    e.Message,
                    "Cannot make more requests!",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void DeleteRequest()
        {
            try
            {
                regularUserController.RemoveRequest(SelectedRequest);

                MessageBox.Show(
                    "You successfully deleted the request!",
                    "Success",
                    MessageBoxButton.OK);

                GoBack();
            }
            catch (NullReferenceException exception)
            {
                MessageBox.Show(
                    "No request selected!",
                    "Error!",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
            TryClose();
        }
    }
}
