﻿using Caliburn.Micro;
using CityHall.BLL.RegularUserBusiness;
using CityHall.Models;

namespace CityHall.ViewModels
{
    public class AllUserRequestsViewModel : Screen
    {
        private readonly IWindowManager windowManager;
        private readonly  RegularUserController regularUserController;
        private readonly RegularUser regularUser;

        public BindableCollection<Request> Requests { get; set; }

        public AllUserRequestsViewModel(RegularUser user)
        {
            windowManager = new WindowManager();

            regularUser = user;

            IRegularUserService regularUserService = new RegularUserService();
            regularUserController = new RegularUserController(regularUserService);

            Requests = new BindableCollection<Request>(regularUserController.GetAllRequests(regularUser));
        }

        public void GoBack()
        {
            windowManager.ShowWindow(new UserOperationsViewModel(regularUser));
            TryClose();
        }
    }
}
