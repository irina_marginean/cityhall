﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityHall.Models
{
    public class Document
    {
        [Key]
        public Guid Id { get; set; }
        public string Type { get; set; }
    }
}
