﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityHall.Models
{
    public class Property
    {
        [Key]
        public Guid Id { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Apartment { get; set; }
        public RegularUser User { get; set; }
    }
}
