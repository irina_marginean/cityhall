﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityHall.Models
{
    public class Request
    {
        [Key]
        public Guid Id { get; set; }
        public RegularUser User { get; set; }
        public Property Property { get; set; }
        public Document Document { get; set; }
        public string Status { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
