﻿using System.Collections.Generic;

namespace CityHall.Models
{
    public class RegularUser : User
    {
        public ICollection<Property> Properties { get; set; }
        public ICollection<Request> Requests { get; set; }
    }
}
