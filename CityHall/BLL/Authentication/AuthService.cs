﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CityHall.DAL;
using CityHall.Models;
using static System.String;

namespace CityHall.BLL.Authentication
{
    public class AuthService : IAuthService
    {
        public string ErrorMessage { get; set; }

        public User Login(string email, string password)
        {
            RegularUser foundUser = RepositoryLocator.RegularUserRepository.GetAll().
                            FirstOrDefault(u => u.Email.Equals(email) && 
                                                u.Password.Equals(password));

            if (foundUser == null)
            {
                Admin foundAdmin = RepositoryLocator.AdminRepository.GetAll().
                    FirstOrDefault(u => u.Email.Equals(email) && 
                                        u.Password.Equals(password));

                return foundAdmin;
            }

            return foundUser;
        }

        public void Register(RegularUser user)
        {
            RepositoryLocator.RegularUserRepository.Insert(user);
        }

        public bool UserExists(User user)
        {
            User foundUser = GetAllUsers().
                             FirstOrDefault(u => u.Id == user.Id &&
                                                 u.Email.Equals(user.Email));

            if (foundUser == null)
            {
                return false;
            }

            return true;
        }

        public bool CanLogin(string username, string password)
        {
            return !IsNullOrEmpty(username) && !IsNullOrEmpty(password);
        }

        public bool IsUserValid(User user)
        {
            ErrorMessage = Empty;

            if (user.Email == null)
            {
                ErrorMessage = "No email was set!\n";
                return false;
            }

            if (!new EmailAddressAttribute().IsValid(user.Email))
            {
                ErrorMessage += "Email address lacks domain!\n";
                return false;
            }

            if (user.Password.Length < 4 || user.Password.Length > 15)
            {
                ErrorMessage += "Password must have at least 5 and at most 14 characters!";
                return false;
            }

            return true;
        }

        private IEnumerable<User> GetAllUsers()
        {
            IEnumerable<User> regularUsers = RepositoryLocator.RegularUserRepository.GetAll();
            IEnumerable<User> admins = RepositoryLocator.AdminRepository.GetAll();

            IEnumerable<User> allUsers = regularUsers.Concat(admins);

            return allUsers;
        }
    }
}
