﻿using CityHall.Models;

namespace CityHall.BLL.Authentication
{
    public interface IAuthService
    {
        string ErrorMessage { get; set; }
        User Login(string email, string password);
        void Register(RegularUser user);
        bool UserExists(User user);
        bool IsUserValid(User user);
        bool CanLogin(string username, string password);
    }
}
