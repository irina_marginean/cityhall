﻿using System;
using CityHall.Models;

namespace CityHall.BLL.Authentication
{
    public class AuthController
    {
        private readonly IAuthService authService;

        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }

        public User Login(string email, string password)
        {
            if (authService.CanLogin(email, password))
            {
                return authService.Login(email, password);
            }

            return null;
        }

        public void Register(User user)
        {
            if (!authService.IsUserValid(user))
            {
                throw new InvalidOperationException(authService.ErrorMessage);
            }

            if (authService.UserExists(user))
            {
                throw new InvalidOperationException("User already exists!");
            }

            authService.Register((RegularUser)user);
        }
    }
}
