﻿using System.Collections.Generic;
using System.Linq;
using CityHall.Models;

namespace CityHall.BLL.AdminBusiness
{
    public class AdminController
    {
        private readonly IAdminService adminService;

        public AdminController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IEnumerable<User> GetAllUsers()
        {
            return adminService.GetAllUsers();
        }

        public void AddDocument(Document document)
        {
            adminService.AddDocument(document);
        }

        public void RemoveDocument(Document document)
        {
            RemoveRequestsPerDocument(document);

            adminService.RemoveDocument(document);
        }

        private void RemoveRequestsPerDocument(Document document)
        {
            IEnumerable<Request> requestsForDocument = GetAllRequests().Where(r => r.Document.Id == document.Id);

            foreach (var request in requestsForDocument)
            {
                RemoveRequest(request);
            }
        }

        public IEnumerable<Request> GetAllRequests()
        {
            return adminService.GetAllRequests();
        }

        public void ApproveRequest(Request request)
        {
            adminService.ApproveRequest(request);
        }

        public void DenyRequest(Request request)
        {
            adminService.DenyRequest(request);
        }

        public void RemoveRequest(Request request)
        {
            adminService.RemoveRequest(request);
        }

        public IEnumerable<Document> GetAllDocuments()
        {
            return adminService.GetAllDocuments();
        }
    }
}
