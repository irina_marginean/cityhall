﻿using System;
using System.Collections.Generic;
using System.Linq;
using CityHall.DAL;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.BLL.AdminBusiness
{
    public class AdminService : IAdminService
    {
        public IEnumerable<User> GetAllUsers()
        {
            IEnumerable<User> regularUsers = RepositoryLocator.RegularUserRepository.GetAll();
            IEnumerable<User> admins = RepositoryLocator.AdminRepository.GetAll();

            IEnumerable<User> allUsers = regularUsers.Concat(admins);

            return allUsers;
        }

        public void AddDocument(Document document)
        {
            RepositoryLocator.DocumentRepository.Insert(document);
        }

        public void RemoveDocument(Document document)
        {
            RepositoryLocator.DocumentRepository.Delete(document);
        }

        public IEnumerable<Request> GetAllRequests()
        {
            DbSet<Request> requests = RepositoryLocator.RequestRepository.GetAll();

            IQueryable<Request> requestQuery = requests.Include(r => r.User);
            IQueryable<Request> requestQuery2 = requestQuery.Include(r => r.Document);
            IEnumerable<Request> requestList = requestQuery2.Include(r => r.Property);

            return requestList;
        }

        public void ApproveRequest(Request request)
        {
            Request requestToApprove = RepositoryLocator.
                                       RequestRepository.
                                       GetAll().
                                       FirstOrDefault(r => r.Id == request.Id);

            if (requestToApprove == null)
            {
                throw new NullReferenceException("No such request was found!");
            }

            if (request.Status.Equals("APPROVED"))
            {
                throw new InvalidOperationException("Request already approved!");
            }

            requestToApprove.Status = "APPROVED";

            RepositoryLocator.RequestRepository.Update(requestToApprove);

        }

        public void DenyRequest(Request request)
        {
            Request requestToDeny = RepositoryLocator.
                RequestRepository.
                GetAll().
                FirstOrDefault(r => r.Id == request.Id);

            if (requestToDeny == null)
            {
                throw new NullReferenceException("No such request was found!");
            }

            if (request.Status.Equals("DENIED"))
            {
                throw new InvalidOperationException("Request already denied!");
            }

            requestToDeny.Status = "DENIED";

            RepositoryLocator.RequestRepository.Update(requestToDeny);
        }

        public void RemoveRequest(Request request)
        {
            Request requestToApprove = RepositoryLocator.
                RequestRepository.
                GetAll().
                FirstOrDefault(r => r.Id == request.Id);

            if (requestToApprove == null)
            {
                throw new NullReferenceException("No such request was found!");
            }

            RepositoryLocator.RequestRepository.Delete(requestToApprove);
        }

        public IEnumerable<Document> GetAllDocuments()
        {
            return RepositoryLocator.DocumentRepository.GetAll();
        }
    }
}
