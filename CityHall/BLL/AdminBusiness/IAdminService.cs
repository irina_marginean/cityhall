﻿using System.Collections.Generic;
using CityHall.Models;

namespace CityHall.BLL.AdminBusiness
{
    public interface IAdminService
    {
        IEnumerable<User> GetAllUsers();
        void AddDocument(Document document);
        void RemoveDocument(Document document);
        IEnumerable<Request> GetAllRequests();
        void ApproveRequest(Request request);
        void DenyRequest(Request request);
        void RemoveRequest(Request request);
        IEnumerable<Document> GetAllDocuments();
    }
}
