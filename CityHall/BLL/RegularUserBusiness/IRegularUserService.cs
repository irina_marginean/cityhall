﻿using System.Collections.Generic;
using CityHall.Models;

namespace CityHall.BLL.RegularUserBusiness
{
    public interface IRegularUserService
    { 
        void AddProperty(Property property);
        void RemoveProperty(RegularUser user, Property property);
        IEnumerable<Request> GetAllRequests(RegularUser user);
        void AddRequest(Request request);
        void UpdateRequest(Request request);
        void RemoveRequest(Request request);
        bool CanMakeRequest(Request request);
        IEnumerable<Property> GetAllProperties(RegularUser user);
    }
}
