﻿using System;
using System.Collections.Generic;
using System.Linq;
using CityHall.Models;

namespace CityHall.BLL.RegularUserBusiness
{
    public class RegularUserController
    {
        private readonly IRegularUserService regularUserService;

        public RegularUserController(IRegularUserService regularUserService)
        {
            this.regularUserService = regularUserService;
        }

        public void AddProperty(Property property)
        {
            regularUserService.AddProperty(property);
        }

        public void RemoveProperty(RegularUser user, Property property)
        {
           RemoveRequestsPerProperty(user, property);

            regularUserService.RemoveProperty(user, property);
        }

        public IEnumerable<Request> GetAllRequests(RegularUser user)
        {
            return regularUserService.GetAllRequests(user);
        }

        public void AddRequest(Request request)
        {
            if (!regularUserService.CanMakeRequest(request))
            {
                throw new InvalidOperationException("This user cannot have any more requests of this type this year for this property!");
            }

            regularUserService.AddRequest(request);
        }

        public void UpdateRequest(Request request)
        {
            if (!regularUserService.CanMakeRequest(request))
            {
                throw new InvalidOperationException("This user cannot have any more requests of this type this year for this property!");
            }

            regularUserService.UpdateRequest(request);
        }

        public void RemoveRequest(Request request)
        {
           regularUserService.RemoveRequest(request);
        }

        public IEnumerable<Property> GetAllProperties(RegularUser user)
        {
            return regularUserService.GetAllProperties(user);
        }

        private void RemoveRequestsPerProperty(RegularUser user, Property property)
        {
            IEnumerable<Request> requestsForProperty = GetAllRequests(user).
                Where(r => r.Property.Id == property.Id);

            foreach (Request request in requestsForProperty)
            {
                RemoveRequest(request);
            }
        }
    }
}
