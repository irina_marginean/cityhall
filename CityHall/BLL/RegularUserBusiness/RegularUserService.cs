﻿using System;
using System.Collections.Generic;
using System.Linq;
using CityHall.DAL;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.BLL.RegularUserBusiness
{
    public class RegularUserService : IRegularUserService
    {
        public void AddProperty(Property property)
        {
            RepositoryLocator.PropertyRepository.Insert(property);
        }

        public void RemoveProperty(RegularUser user, Property property)
        {
            Property propertyToRemove = RepositoryLocator.
                                        PropertyRepository.
                                        GetAll().
                                        FirstOrDefault(p => p.User == user);

            if (propertyToRemove == null)
            {
                throw new NullReferenceException("No such property was found!");
            }

            RepositoryLocator.PropertyRepository.Delete(property);
        }

        public IEnumerable<Request> GetAllRequests(RegularUser user)
        {
            DbSet<Request> requests = RepositoryLocator.RequestRepository.GetAll();

            IQueryable<Request> requestQuery = requests.Include(r => r.User);
            IQueryable<Request> requestQuery2 = requestQuery.Include(r => r.Document);
            IEnumerable<Request> requestList = requestQuery2.Include(r => r.Property);

            return  requestList.Where(p => p.User.Id == user.Id);
        }

        public void AddRequest(Request request)
        {
            RepositoryLocator.RequestRepository.Insert(request);
        }

        public void UpdateRequest(Request request)
        {
            Request requestToUpdate =   RepositoryLocator.
                                        RequestRepository.
                                        GetAll().
                                        FirstOrDefault(p => p.User.Id == request.User.Id);

            if (requestToUpdate == null)
            {
                throw new NullReferenceException("No such request was found!");
            }

            RepositoryLocator.RequestRepository.Update(request);
        }

        public void RemoveRequest(Request request)
        {
            Request requestToRemove =   RepositoryLocator.
                                        RequestRepository.
                                        GetAll().
                                        FirstOrDefault(p => p.User.Id == request.User.Id);

            if (requestToRemove == null)
            {
                throw new NullReferenceException("No such request was found!");
            }

            RepositoryLocator.RequestRepository.Delete(request);
        }

        public bool CanMakeRequest(Request request)
        {
            try
            {
                int numberOfRequestsPerTypePerYear = 
                        GetAllRequests(request.User).
                        Where(r => r.User.Id == request.User.Id).
                        Where(r => r.Property.Id == request.Property.Id).
                        Where(r => r.Document.Type.Equals(request.Document.Type)).
                        Count(r => r.DateCreated.Year == request.DateCreated.Year);

                if (numberOfRequestsPerTypePerYear >= 0 && numberOfRequestsPerTypePerYear < 3)
                {
                    return true;
                }

                return false;
            }
            catch(ArgumentNullException)
            {
                return false;
            }
        }

        public IEnumerable<Property> GetAllProperties(RegularUser user)
        {
            return  RepositoryLocator.
                    PropertyRepository.
                    GetAll().
                    Where(p => p.User.Id == user.Id);
        }
    }
}
