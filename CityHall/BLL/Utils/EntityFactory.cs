﻿using System;
using System.Collections.Generic;
using CityHall.Models;

namespace CityHall.BLL.Utils
{
    public static class EntityFactory
    {
        public static RegularUser CreateDefaultRegularUser()
        {
            RegularUser user = new RegularUser
            {
                Id = Guid.NewGuid(),
                Email = "user@gmail.com",
                FirstName = "Jane",
                LastName = "Doe",
                Password = "password",
                Properties = new List<Property>(),
                Requests = new List<Request>()
            };

            return user;
        }

        public static Property CreateDefaultProperty(RegularUser user)
        {
            Property property = new Property
            {
                Id = Guid.NewGuid(),
                Street = "Oxford",
                Number = "1-3-5",
                Apartment = "1A",
                User = user,
            };

            return property;
        }

        public static Document CreateDefaultDocument()
        {
            Document document = new Document
            {
                Id = Guid.NewGuid(),
                Type = "ID Card",
            };

            return document;
        }

        public static Request CreateDefaultRequest(RegularUser user, Property property, Document document)
        {
            Request request = new Request
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.Now,
                Document = document,
                Property = property,
                Status = "WAITING",
                User = user
            };

            return request;
        }
    }
}
