﻿using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        private readonly DataContext dataContext;
        private readonly DbSet<T> dbSet;

        public BaseRepository(DataContext dataContext)
        {
            this.dataContext = dataContext;
            dbSet = dataContext.Set<T>();
        }

        public DbSet<T> GetAll()
        {
            return dbSet;
        }

        public void Insert(T entity)
        {
            dbSet.Add(entity);

            dataContext.SaveChanges();
        }

        public void Update(T entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            dataContext.Entry(entityToUpdate).State = EntityState.Modified;

            dataContext.SaveChanges();
        }

        public void Delete(T entityToDelete)
        {
            if (dataContext.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);

            dataContext.SaveChanges();
        }
    }
}
