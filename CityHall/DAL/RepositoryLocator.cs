﻿using CityHall.Models;

namespace CityHall.DAL
{
    public static class RepositoryLocator
    {
        private static readonly DataContext DataContext = new DataContext();

        private static IRepository<RegularUser> regularUserRepository;
        private static IRepository<Admin> adminRepository;
        private static IRepository<Property> propertyRepository;
        private static IRepository<Document> documentRepository;
        private static IRepository<Request> requestRepository;

        public static IRepository<RegularUser> RegularUserRepository =>
            regularUserRepository ??= new BaseRepository<RegularUser>(DataContext);

        public static IRepository<Admin> AdminRepository =>
            adminRepository ??= new BaseRepository<Admin>(DataContext);

        public static IRepository<Property> PropertyRepository =>
            propertyRepository ??= new BaseRepository<Property>(DataContext);

        public static IRepository<Document> DocumentRepository =>
            documentRepository ??= new BaseRepository<Document>(DataContext);

        public static IRepository<Request> RequestRepository =>
            requestRepository ??= new BaseRepository<Request>(DataContext);
    }
}
