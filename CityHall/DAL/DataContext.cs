﻿using CityHall.DAL.Seeders;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<RegularUser> RegularUsers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Request> Requests { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=city_hall.sqlite");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RegularUser>()
                .HasMany(ru => ru.Properties)
                .WithOne(p => p.User)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Property>()
                .HasOne(x => x.User)
                .WithMany(p => p.Properties)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<RegularUser>()
                .HasMany(ru => ru.Requests)
                .WithOne(r => r.User)
                .OnDelete(DeleteBehavior.Cascade);

            DataContextSeeder.Seed(modelBuilder);
        }
    }
}
