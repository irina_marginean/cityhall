﻿using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL
{
    public interface IRepository<T> where T : class
    {
        DbSet<T> GetAll();
        void Insert(T entity);
        void Update(T entityToUpdate);
        void Delete(T entityToDelete);
    }
}
