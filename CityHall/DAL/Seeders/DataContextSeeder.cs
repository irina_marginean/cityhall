﻿using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL.Seeders
{
    internal static class DataContextSeeder
    {
        internal static void Seed(ModelBuilder modelBuilder)
        {
            RegularUserSeeder.Seed(modelBuilder);
            AdminSeeder.Seed(modelBuilder);
            DocumentSeeder.Seed(modelBuilder);
        }
    }
}
