﻿using System;
using System.Collections.Generic;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL.Seeders
{
    internal static class AdminSeeder
    { 
        internal static List<Admin> AdminList { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Admin admin in AdminList)
            {
                modelBuilder.Entity<Admin>().HasData(admin);
            }
        }

        private static List<Admin> GetData()
        {
            List<Admin> adminList = new List<Admin>
            {
                new Admin
                {
                    Id = new Guid("71df78ea-ffe1-4174-a4bb-25bff76afe1b"),
                    Email = "admin@cityhall.com",
                    FirstName = "John",
                    LastName = "Doe",
                    Password = "admin"
                },
                new Admin
                {
                    Id = new Guid("c31aec49-5dcc-4a65-84bf-83075c543da2"),
                    Email = "admin_two@cityhall.com",
                    FirstName = "Peppa",
                    LastName = "Pig",
                    Password = "admin"
                }
            };

            return adminList;
        }
    }
}
