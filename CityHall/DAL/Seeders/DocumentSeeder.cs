﻿using System;
using System.Collections.Generic;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL.Seeders
{
    internal static class DocumentSeeder
    {
        internal static List<Document> DocumentList { get; } = GetData();

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (Document document in DocumentList)
            {
                modelBuilder.Entity<Document>().HasData(document);
            }
        }

        private static List<Document> GetData()
        {
            List<Document> adminList = new List<Document>
            {
                new Document
                {
                    Id = Guid.NewGuid(),
                    Type = "ID Card"
                },
                new Document
                {
                    Id = Guid.NewGuid(),
                    Type = "Birth Certificate"
                }
            };

            return adminList;
        }
    }
}
