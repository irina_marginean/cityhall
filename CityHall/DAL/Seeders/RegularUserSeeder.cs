﻿using System;
using System.Collections.Generic;
using CityHall.Models;
using Microsoft.EntityFrameworkCore;

namespace CityHall.DAL.Seeders
{
    internal static class RegularUserSeeder
    {
        internal static List<RegularUser> UserList = new List<RegularUser>
        {
            new RegularUser
            {
                Id = new Guid("00535663-5dde-45ce-ae64-e984799d5d3f"),
                Email = "irina@gmail.com",
                FirstName = "Irina",
                LastName = "Marginean",
                Password = "password",
            },
            new RegularUser
            {
                Id = new Guid("d447d1b6-131b-461d-b1b5-b9d69f642ebc"),
                Email = "gigel@gmail.com",
                FirstName = "Gigel",
                LastName = "Costel",
                Password = "password",
            }
        };

        internal static void Seed(ModelBuilder modelBuilder)
        {
            foreach (RegularUser user in UserList)
            {
                modelBuilder.Entity<RegularUser>().HasData(user);
            }
        }
    }
}
